package com.github.marcopollivier.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Created by mpoll on 26/05/2017.
 */
@Service
public class HelloMessageService {

    @Value("${name.default:unknown}")
    private String name;

    public String getMessage() {
        return getMessage(name);
    }


    public String getMessage(String name) {
        return "Hello " + name;
    }
}
